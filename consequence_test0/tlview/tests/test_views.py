from django.conf import settings
from django.test import RequestFactory

from ..views import start_view


class TestStartView:
    def test_start(self, rf: RequestFactory):
        request = rf.get("/fake-url/")
        response = start_view(request)
        assert response.status_code == 200
        assert "tl_auth_link" in response.context_data
        assert response.context_data["tl_auth_link"] == settings.TL_AUTH_LINK
        response.render()
        assert "Connect" in response.rendered_content
