from django.urls import resolve, reverse


def test_start():
    assert reverse("tlview:start") == "/tl/start/"
    assert resolve("/tl/start/").view_name == "tlview:start"
