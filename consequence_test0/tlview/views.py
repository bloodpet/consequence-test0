from django.conf import settings
from django.views.generic import TemplateView
from django.shortcuts import redirect

from .clients import TrueLayerClient


class StartView(TemplateView):
    template_name = "tlview/start.html"

    def get_context_data(self, **kwargs):
        kwargs["tl_auth_link"] = settings.TL_AUTH_LINK
        return super().get_context_data(**kwargs)


def callback_view(request):
    tlc = TrueLayerClient(settings.TL_CLIENT_ID, settings.TL_CLIENT_SECRET)
    tlc.exchange_code(request.GET["code"])
    # TODO: Redirect somewhere where you get the list of accounts
    return redirect("/")


start_view = StartView.as_view()
