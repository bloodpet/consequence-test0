from django.urls import path

from .views import callback_view, start_view

app_name = "tlview"
urlpatterns = [
    path("start/", view=start_view, name="start"),
    path("callback/", view=callback_view, name="callback"),
]
