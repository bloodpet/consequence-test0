import requests


class TrueLayerClient:
    def __init__(self, client_id, secret):
        self.client_id = client_id
        self.secret = secret

    def exchange_code(self, code):
        response = requests.post(
            "https://auth.truelayer-sandbox.com/connect/token",
            data={
                "grant_type": "authorization_code",
                "client_id": self.client_id,
                "client_secret": self.secret,
                "redirect_uri": "http://localhost:8000/tl/callback/",
                "code": code,
            },
        )
        response.raise_for_status()
        data = response.json()
        self.refresh_token = data["refresh_token"]
        self._parse_jwt_response(data)

    def renew_access_token(self):
        if not self.refresh_token:
            raise ValueError("No saved refresh_token")

        response = requests.post(
            "https://auth.truelayer.com/connect/token",
            data={
                "grant_type": "refresh_token",
                "client_id": self.client_id,
                "client_secret": self.secret,
                "refresh_token": self.refresh_token,
            }
        )
        response.raise_for_status()
        data = response.json()
        self._parse_jwt_response(data)

    def list_all_accounts(self):
        response = requests.get(
            "https://api.truelayer.com/data/v1/accounts",
            headesr=self.jwt_header
        )

    def _parse_jwt_response(self, data):
        self.access_token = data["access_token"]
        self.expiry = data["expires_in"]

    @property
    def jwt_header(self):
        # @TODO: check expiry & refresh if expired
        return {
            "authorization": f"Bearer: {self.access_token}"
        }
